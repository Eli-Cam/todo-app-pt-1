import React, { Component } from "react"
import TodoItem from "../todoitem/TodoItem"
class TodoList extends Component {
  constructor(props) {
    super(props)
    this.state = {
        incomplete: true
    }
    this.handleIncomplete = this.handleIncomplete.bind(this)
  }

  handleIncomplete() {
    this.setState(state => ({
      incomplete: !state.incomplete
    }))
  }

  render() {
    return ( 
      <section className="main">
        <ul className={this.state.incomplete ? "todo-list" : ""}>
          {this.props.todos.map((todo) => (
            <TodoItem toggle={this.props.handleToggle} handleDelete={this.props.handleDelete} id={todo.id} title={todo.title} key={todo.id} completed={todo.completed} />
          ))}
        </ul>
      </section>
    )
  }
}

export default TodoList
