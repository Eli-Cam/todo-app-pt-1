import React, { Component } from 'react';

class TodoItem extends Component {
    state = {
      Completed: false,
      newCompleted: true
    }

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input onClick={() => this.props.toggle(this.props.id)} className="toggle" type="checkbox" checked={this.props.completed} />
          <label>{this.props.title}</label>
          <button onClick={() => this.props.handleDelete(this.props.id)} className="destroy" />
        </div>
      </li>
    );
  }
}

export default TodoItem;

