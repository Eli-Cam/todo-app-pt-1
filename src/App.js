import React, { Component } from "react";
import todosList from "./todos.json";
import TodoList from "./components/todolist/TodoList"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      todos: todosList,
      todoItem: '',
      incomplete: true,
      clearComplete: true,
    }
    this.handleClearComplete = this.handleClearComplete.bind(this);
  }

  handleChange = (event) => {
    let todoItem = event.target.value
    this.setState({
      todoItem: todoItem
    })
  }

  addTodo = (todo) => {
    this.setState((state, props) => ({
      todos: [...state.todos, todo]
    }))
  }

  handleSubmit = (event) => {
    event.preventDefault()
    let newTodo = {
      "userId": 1,
      "id": 1,
      "title": this.state.todoItem,
      "completed": false
    }
    this.addTodo(newTodo)
    this.setState({
      todoItem: '',
    })
  }

  handleComplete() {
    this.setState(state => ({
      newComplete: !state.newComplete
    }))
  }

  handleClearComplete() {
    this.setState(state => ({
      clearComplete: !state.clearComplete
    }))
  }

  handleDelete = todoId => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.id !== todoId
    )
    this.setState({ todos: newTodos })
  }

  handleToggle = todoId => {
    const newTodos = this.state.todos.map(
      todoItem => {
        if(todoItem.id === todoId){
          todoItem.completed = !todoItem.completed
        }
      return todoItem
      }
    )
    this.setState({ todos: newTodos })
  }

  handleClearComplete () {
    const newArray = this.state.todos.filter(
      todoItem => todoItem.completed === false
    )
    this.setState({ todos: newArray})
  }

  handleAll () {
    const allTodos = this.state.todos.map(
      todoItem => todoItem.id === this.state.todos
    )
    this.setState({ todos: allTodos })
  }

  handleActive () {
    const activeTodos = this.state.todos.filter(
      todoItem => todoItem.completed === true
    )
    this.setState({ todos: activeTodos })
  }

  handleComplete() {
    const completedTodos = this.state.todos.filter(
      todoItem => todoItem.completed === false
    )
    this.setState({ todos: completedTodos })
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <form className="SignUpForm" onSubmit={this.handleSubmit}>
            <h1>todos</h1>
            <input
            className="newTodo" 
            placeholder="What needs to be done?"
            autoFocus
            value={this.state.todoItem} 
            onChange={this.handleChange}
            />
          </form>
        </header>
        <TodoList handleDelete={this.handleDelete} handleToggle={this.handleToggle} addTodo={this.addTodo} todos={this.state.todos}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
            <Router>
              <Route 
                exact
                path="/" 
                render={() => (
                  <TodoList
                    todos={this.state.todos}
                    handleToggle={this.handleToggle}
                  />
                )}
              />
              <Route
                exact
                path="/active"
                render={() => (
                  <TodoList
                  todos={this.state.todos.filter(todoItem => todoItem.completed === false)}
                  handleToggle={this.handleToggle}
                  />
                )}/>
              <Route
                exact
                path="/complete"
                render={() => (
                  <TodoList
                  todos={this.state.todos.filter(todoItem => todoItem.completed === true)}
                  handleToggle={this.handleToggle}
                  />
                )}/>
            <ul>
              <li>
                <Link exact to="/" activeClassName="selected">All</Link>
              </li>
              <li>
                <Link exact to="/active" activeClassName="selected">Active</Link>
              </li>
              <li>
                <Link exact to="/complete" activeClassName="selected">Complete</Link>
              </li>
            </ul>
            </Router>
          </span>
          <button onClick={this.handleClearComplete} className="clear-completed" >Clear completed</button>
        </footer>
      </section>
    );
  }
}

export default App;